from django.urls import path

from .views import (
    list_todolist,
    detail_todolist,
    create_todolist,
    update_todolist,
    delete_todolist,
    create_todoitem,
    update_todoitem,
)


urlpatterns = [
    path("", list_todolist, name="list_todos"),
    path("<int:id>/", detail_todolist, name="show_todolist"),
    path("create/", create_todolist, name="create_todolist"),
    path("<int:id>/edit", update_todolist, name="update_todolist"),
    path("<int:id>/delete", delete_todolist, name="delete_todolist"),
    path("items/create", create_todoitem, name="create_todoitem"),
    path("<int:id>/items/edit", update_todoitem, name="update_todoitem"),
]