from django.shortcuts import render, redirect
from todos.form import TodoListForm, TodoItemForm

# Create your views here.
from .models import (
    TodoList,
    TodoItem,
)

def list_todolist(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos,
    }
    return render(request, "todos/list.html", context)


def detail_todolist(request, id):
    todos_detail = TodoList.objects.get(pk=id)
    context = {
        "todos_detail": todos_detail,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    form = TodoListForm()
    if request.method == "POST" and TodoListForm:
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
        return redirect("show_todolist", id=list.id)
    
    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def update_todolist(request, id):
    if TodoList and TodoListForm:
        instance = TodoList.objects.get(pk=id)
        if request.method == "POST":
            form = TodoListForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("show_todolist", id=id)
        else:
            form = TodoListForm(instance=instance)

    context={
        "form": form,
    }
    return render(request, "todos/update.html", context)


def delete_todolist(request, id):
    todos = TodoList.objects.get(id=id)
    if request.method =="POST":
        todos.delete()
        return redirect("list_todos")

    context = {
        "todos": todos,
    }
    return render(request, "todos/delete.html", context)


def create_todoitem(request):
    form = TodoItemForm()

    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("show_todolist", id=item.list.id)

    context = {
        "form": form,
    }
    return render(request, "todos/createitem.html", context)


def update_todoitem(request, id):
    instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=instance)
        if form.is_valid():
            item = form.save()
            return redirect("show_todolist", id=item.list.id)
    else:
        form = TodoItemForm(instance=instance)

    context = {
        "form": form,
    }
    return render(request, "todos/updateitem.html", context)